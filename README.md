# EPLAN-tools

Målet med dette prosjektet er å samle data, verktøy og instrukser som er
tilpasset norske EPLAN-brukere.

Det brukes en del [Jinja](https://palletsprojects.com/p/jinja/) syntaks i dette
prosjektet. De som bidrar i prosjektet har fordel av å vite hva Jinja er.

# Rutinegenerator

Filer som ligger under **prosedyrer**-mappen skal være skrevet med tanke på alle norske bedrifter som bruker EPLAN.
Konkrete firmanavn og andre variabler merkes med [Jinja](https://palletsprojects.com/p/jinja/) syntaks.

Et verktøy for å flette inn konkret firmainfo fra **prosedyrer**-mappen er tilgjengelig fra nettsiden [apent-norsk-industriverksted.gitlab.io/EPLAN-tools](https://apent-norsk-industriverksted.gitlab.io/EPLAN-tools/).

# Skript

## Roadmap for skript

Oppgave                         | Status
--------------------------------|---
Update and export               | Startet
Test av systemdata              | Ikke Startet
Test av produksjonsunderlag     | Ikke Startet
Project check-in                | Git, GitLab og Visual studio code er det beste vi har
Project check-out               | Git, GitLab og Visual studio code er det beste vi har
System data check-in            | Git, GitLab og Visual studio code er det beste vi har
System data check-out           | Git, GitLab og Visual studio code er det beste vi har

## Update and export

Dette er en oppgave som det er verdt å investere i. En automatisert prosess som genererer
produksjonsunderlag sikkrer kvalitet og reduserer tidsbruk.

Det er en utfordring som dukker opp i flerbrukermiljøer. Noen instillinger må publiseres
til alle EPLAN-brukere for at eksporten skal se lik ut for alle brukere. For eksempel
noen "Page filter" og "PDF export scheme". Det er mulig å lage en add-on som hjelper
til med å synkronisere slike instillinger.

## Test av systemdata

## Test av prosjektdata

# Add-ons mappen

Samling av konfigurasjoner og grunndata er her samlet i EPLAN sitt **Add-on** format.

Her er en liten instruks for hvordan mappen kan aktiveres:

- Flytt mappen til egen PC
- EPLAN, Utilities, Add-ons
- Åpne install.xml fra Add-ons mappen
- EPLAN vil nå hente inn data og konfigurasjoner automatisk

# Event overview

Det er mulig at det finnes en komplett liste over events i EPLAN. Gi gjerne beskjed hvis du finner en. Tabellen under har noen events.

Event name                                  | Description
--------------------------------------------|---
onMainStart                                 | EPLAN startup
onMainEnd                                   | EPLAN shutdown
onActionEnd.String.XPrjActionProjectOpen    | Before project is opening
Eplan.EplApi.OnPostOpenProject              | After project is opened
onActionStart.String.XPrjActionProjectClose | Before project is closing

# Bidra til bedre grunndata

Det er masse å gjøre om du ønsker å bidra. Det er enkelt å bidra:

- Registrer deg som bruker hos gitlab.com
- Se etter registrerte "Issues", eller opprett et "Issue" om du har ønsker
- Foreslå endringer med en "Pull request"

Kontakt Håvard på havard.line@lts.no eller 97104347 for en innføring

# Resurser

Endringer i EPLAN 2.9:
https://www.eplan.help/help/platform/2.9/en-US/help/EPLAN_Help.htm#htm/news_p_einleitung_vorwort_2_9.htm
