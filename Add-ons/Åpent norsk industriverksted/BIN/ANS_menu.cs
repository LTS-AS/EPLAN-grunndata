using System;
using System.Text;
using System.Xml;


namespace Apent_norsk_industriverksted
{
    class Menu_elements
    {
        [DeclareAction("my_first_action")]
        public void my_first_action()
        {
            String username_long = String.Empty;
            Eplan.EplApi.Base.Settings settings_handler = new Eplan.EplApi.Base.Settings();
            username_long = settings_handler.GetStringSetting("USER.TrDMProject.UserData.Longname", 0);
            MessageBox.Show(username_long, "First action, showing USER.TrDMProject.UserData.Longname");
            return;
        }

        [DeclareMenu]
        public void MenuFunction()
        {
            Eplan.EplApi.Gui.Menu menu_object = new Eplan.EplApi.Gui.Menu();

            uint main_menu_handle = menu_object.AddMainMenu(
                "Åpent norsk industriverksted", // main menu name
                "Utilities", // closest main meniu
                "Archive project to GitLab", // entry name,
                "my_first_action", // action,
                "Archiving project to GitLab", // status text,
                1); // Entry position (1 = back or 0 = front)

            menu_object.AddMenuItem(
                "Open project from GitLab", 
                "my_first_action", // action,
                "Opening project from GitLab", 
                main_menu_handle, 
                1,
                false, 
                false);
            return;
        }
    }
}