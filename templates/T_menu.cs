public class CustomMenu
{
      // TOTO: Find a way to read separate archivePath from this file.
      private string archivePath = @"L:\EPLAN\worker\input\";
      [DeclareAction("backupAction")]
      public void backupAction()
      {
            // The onMainStart event is trigging on EPLAN startup.
            ActionCallingContext backupContext = new ActionCallingContext();
		backupContext.AddParameter("BACKUPMEDIA", "DISK");
		backupContext.AddParameter("BACKUPMETHOD", "BACKUP");
		backupContext.AddParameter("COMPRESSPRJ", "0");
		backupContext.AddParameter("INCLEXTDOCS", "1");
		backupContext.AddParameter("BACKUPAMOUNT", "BACKUPAMOUNT_ALL");
		backupContext.AddParameter("INCLIMAGES", "1");
		backupContext.AddParameter("LogMsgActionDone", "true");
		backupContext.AddParameter("DESTINATIONPATH", archivePath);
		backupContext.AddParameter("ARCHIVENAME", "$(PROJECTNAME) "+System.DateTime.Now.ToString("yyyyMMdd"));
		backupContext.AddParameter("TYPE", "PROJECT");
            new CommandLineInterpreter().Execute("backup",backupContext);
            return;
      }

      [DeclareMenu]
      public void MenuFunction()
      {

           Eplan.EplApi.Gui.Menu oMenu = new Eplan.EplApi.Gui.Menu();
           oMenu.AddMenuItem("EPLAN-tools backup","backupAction");
           return;

      }
     
}