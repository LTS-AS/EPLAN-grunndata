using System.Windows.Forms;
using Eplan.EplApi.Scripting;
 
public class Template_scripts
{
    ActionCallingContext context = new ActionCallingContext();
    bool result;

    [Start]
    public void start()
    {
        // The Start event is trigged when the run script command is executed.
        loadScripts();
        return;
    }

    [DeclareEventHandler("onMainStart")]
    public void onMainStart()
    {
        // The onMainStart event is trigging on EPLAN startup.
        loadScripts();
        return;
    }

    private void loadScripts()
    {
        // This function loads relevant scripts for the EPLAN-tools package

        context = new ActionCallingContext();
        context.AddParameter("ScriptFile",@"$(MD_SCRIPTS)\T_init.cs");
		result = new CommandLineInterpreter().Execute("RegisterScript",context);

        context = new ActionCallingContext();
        context.AddParameter("ScriptFile",@"$(MD_SCRIPTS)\T_menu.cs");
		result = new CommandLineInterpreter().Execute("RegisterScript",context);

        return;
    }

}
