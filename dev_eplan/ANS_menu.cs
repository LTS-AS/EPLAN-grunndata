// using System;
using System.Text;
using System.Xml;
using System.IO;
// using System.Windows.Forms;

// using Eplan.EplApi.Base;
// using Eplan.EplApi.ApplicationFramework;
// using Eplan.EplApi.Gui;

namespace Apent_norsk_industriverksted
{
    class Menu_elements
    {
        public string exportPdfGroups(string[] pdfExportSchemes, string strProjectName, string exportFolder="", string msg = "")
        {
            // Exporting PDF groups
            ActionCallingContext context;
            bool bResult = true;

            foreach (string pdfExportScheme in pdfExportSchemes)
            {
                // Export PDF
                context = new ActionCallingContext();
                context.AddParameter("type","PDFPROJECTSCHEME");
                context.AddParameter("ProjectName",strProjectName);
                context.AddParameter("exportscheme",pdfExportScheme);
                if(exportFolder!="")
                {
                    context.AddParameter("exportfile", exportFolder);
                }
                context.AddParameter("exportmodel", "0"); //0 = keine Modelle ausgeben
                context.AddParameter("blackwhite", "1"); //1 = PDF wird schwarz-weiss
                context.AddParameter("useprintmargins", "1"); //1 = Druckränder verwenden
                context.AddParameter("readonlyexport", "2"); //1 = PDF wird schreibgeschützt
                context.AddParameter("usesimplelink", "1"); //1 = einfache Sprungfunktion
                context.AddParameter("usezoomlevel", "1"); //Springen in Navigationsseiten
                context.AddParameter("fastwebview", "1"); //1 = schnelle Web-Anzeige
                context.AddParameter("zoomlevel", "1"); //wenn USEZOOMLEVEL auf 1 dann hier Zoomstufe in mm
                bResult = new CommandLineInterpreter().Execute("export",context);
		        msg += bResult.ToString()+" PDF export scheme '"+pdfExportScheme+"'\n";
            }
            return msg; 
        }

        [DeclareAction("checkIn")]
        public void checkIn()
        {
            // These document types are defined in IEC 61355-1:
            // 
            // EAA	Administrative documents
            // EAB	Lists (regarding documents)
            // EAC	Explanatory documents (regarding documents)
            // EAZ	Free for user
            // EBA	Registers
            // EBF	Dispatch, storage and transport documents
            // EBH	Documents regarding changes
            // EBZ	Free for user
            // ECB	Approval documents
            // ECD	Order and delivery documents
            // ECZ	Free for user
            // EDA	Data sheets
            // EDB	Explanatory documents
            // EDC	Instructions and manuals
            // EDD	Technical reports
            // EDZ	Free for user
            // EEA	Legal requirement documents
            // EEB	Standards and regulations
            // EEC	Technical specification / requirement documents
            // EED	Dimensioning documents
            // EEZ	Free for user
            // EFA	Functional overview documents
            // EFB	Flow diagrams
            // EFC	MMI layout documents (MMI = man-machine interface)
            // EFE	Function descriptions
            // EFF	Function diagrams
            // EFP	Signal descriptions
            // EFQ	Setting value documents
            // EFS	Circuitry documents
            // EFT	Software specific documents
            // EFZ	Free for user
            // ELD	On-site location documents
            // ELH	In-building location documents (also applied for ships, aircraft, etc.)
            // ELU	In/on-equipment location documents
            // ELZ	Free for user
            // EMA	Connection documents
            // EMB	Cabling or piping documents
            // EMZ	Free for user
            // EPA	Material lists
            // EPB	Parts lists
            // EPC	Item lists
            // EPD	Product lists and product type lists
            // EPF	Function lists
            // EPL	Location lists
            // EPZ	Free for user
            // EQA	Quality management documents
            // EQB	Safety-describing documents
            // EQC	Quality verifying documents
            // EQZ	Free for user
            // ETA	Planning drawings
            // ETB	Construction drawings
            // ETC	Manufacturing and erection drawings
            // ETL	Arrangement documents
            // ETZ	Free for user
            // EWA	Setting value documents
            // EWT	Logbooks
            // EWZ	Free for user
            
            string msg = "Report from check-in\n\n";
            string selectedProject =  PathMap.SubstitutePath(@"$(P)");
            string[] pdfExportSchemes = {
                "EAA Administrative documents",
                "EED Dimensioning documents",
                "EFS Circuitry documents",
                "EPC Item lists"
                };
            // Export to files to <mounting location>/<document type>.pdf
            msg = exportPdfGroups(pdfExportSchemes, selectedProject, @"$(DOC)\internal\", msg);

            // Project path
            MessageBox.Show(msg);

            // Projektname
            // sSelectedProjectName = Path.GetFileNameWithoutExtension(sSelectedProjectData);
            return;
        }

        [DeclareMenu]
        public void MenuFunction()
        {

            Eplan.EplApi.Gui.Menu menu_object = new Eplan.EplApi.Gui.Menu();

            uint menu_handle = menu_object.AddMainMenu(
                "Åpent norsk industriverksted", // menu name,
                "Utilities", // right side menu name,
                "Check in", // entry name,
                "checkIn", // entry action,
                "Status: exporting PDFs", // status text,
                1); // Entry position (1 = back or 0 = front)

            menu_object.AddMenuItem("Open project from GitLab", "my_first_action", "My first action", menu_handle, 1, false, false);
            //String username_long = String.Empty;
            //Eplan.EplApi.Base.Settings settings_handler = new Eplan.EplApi.Base.Settings();
            //username_long = settings_handler.GetStringSetting("USER.TrDMProject.UserData.Longname", 0);
            //MessageBox.Show(username_long);

            // Eplan.EplApi.Gui.ContextMenu oCon = new Eplan.EplApi.Gui.ContextMenu();
            // Eplan.EplApi.Gui.ContextMenuLocation oConLoc = new Eplan.EplApi.Gui.ContextMenuLocation("Editor.Ged", "");
            // oCon.AddMenuItem(oConLoc, "Test", "MyAction", true, false);

            return;
        }
    }
}