"""
# Usage:
<TODO: add usage>

# Description:
The aim of this file is obtain the same variable format that is used in EPLAN.

"""

import xml.etree.ElementTree as ET
from logging import debug
from os import environ, path
from sqlite3 import connect
from voluptuous import Coerce, Schema, All, Length, REMOVE_EXTRA

CFG_SYS = 'C:/Program Files/EPLAN/Pro Panel/2.9.3/cfg/SystemConfiguration.xml'
# Dictionary of local variables. Currently empty.
# Example: 'SERVER_EPLAN_DATA': 'L:\\EPLAN'
LOCAL_CONFIG={
}
ELEMENTS=['CFG', 'CFG_USER', 'CFG_COMPANY', 'EPLAN_DATA', 'RIGHTS_DB_PATH']

def substitute_path(path, cfg):
    # The substitute_path function also exsists in EPLAN.
    start=path.find("$(")     
    end=path.find(")")
    while start != -1:
        if start != -1 and end != -1:
            debug("Subste path input "+path)
            key = path[start+2:end]
            path = cfg[key] + path[end+1:]
            debug("Substite path output "+path)

        start = path.find("$(", start)
        end = path.find(")", start)
    return path

def eplan_read_sysconf_symbolic():
    # Read XML
    debug("Opening "+CFG_SYS)
    root = ET.parse(CFG_SYS).getroot()
    sysconf_root = root.find("./CAT/.[@name='INSTALL']" \
            "./MOD/.[@name='SYSTEM']" \
            "./LEV1/.[@name='Configuration']" \
            "./LEV2/.[@name='SystemConfiguration']" \
            "./LEV3/.[@name='Standard']" \
            "./LEV4/.[@name='Data']")
    variables = {}
    # Add standard variables
    variables['CFG_USER'] = sysconf_root.find("./Setting/.[@name='UserSettingsPath']/Val").text
    variables['CFG'] =sysconf_root.find("./Setting/.[@name='StationSettingsPath']/Val").text
    variables['CFG_COMPANY']=sysconf_root.find("./Setting/.[@name='CompanySettingsPath']/Val").text
    variables['EPLAN_DATA']=sysconf_root.find("./Setting/.[@name='EplanDataPath']/Val").text
    variables['RIGHTS_DB_PATH']=sysconf_root.find("./Setting/.[@name='RightsDbPath']/Val").text
    # Add local variables
    for key in LOCAL_CONFIG:
        variables[key]=LOCAL_CONFIG[key]

    debug("Found "+ str(len(variables))+" variables")
    return variables

class Database():
    def __init__(self):
        self.connection = connect(path.join(LOCAL_CONFIG['LOCAL_TOOLS'], 'index.sqlite'))
        self.cursor = self.connection.cursor()
        # Define schema for file validation
        self.schema = Schema({
            'sha2_hex': All(str, Length(min=64, max=64)),
            'path_relative': All(str, Length(min=1)),
            'path_base': str,
            'timestamp_edit': Coerce(int),
            }, required=True, extra=REMOVE_EXTRA)
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS cache (
                sha2_hex TEXT NOT NULL,
                path_relative TEXT NOT NULL,
                path_base TEXT NOT NULL,
                timestamp_edit TEXT INTEGER NOT NULL,
                PRIMARY KEY(sha2_hex,path_relative,path_base,timestamp_edit)
            ) WITHOUT ROWID;''')

    def len(self, table: str ='cache'):
        self.cursor.execute('SELECT count(*) FROM '+table)
        return self.cursor.fetchone()[0]

    def save(self, record: dict, table: str = 'cache'):
        file_record = self.schema(record)
        self.cursor.execute("INSERT INTO cache VALUES (?,?,?,?)", [v for k,v in file_record.items()])

class EPLANconf(object):
    def __init__(self):
        self._path_raw = eplan_read_sysconf_symbolic()

    def path_raw(self, key):
        return self._path_raw[key]

    def __len__(self):
        return len(self._path_raw)

    def __getitem__(self, key):
        return substitute_path(self._path_raw[key], self._path_raw)

if __name__=='__main__':
    import logging
    logging.basicConfig(level="DEBUG")
    test = EPLANconf()
    print(len(test))
    print(test['RIGHTS_DB_PATH'])
    print(test.path_raw('RIGHTS_DB_PATH'))
