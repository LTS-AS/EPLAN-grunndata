"""
# Usage:
<TODO: add usage>

# Description:
The EPLAN systemdata tend to accumulate.

The aim of this script is to control and monitor the EPLAN systemdata.

"""

from hashlib import sha256
from logging import debug, warning
from os import path, walk
from variables import EPLANconf, Database

# Reading default folders from eplan
paths = EPLANconf()
# Initiate database connection
db = Database()

COMPANY_CODE = 'LTS'

def calculate_hash(file_path):
    hasher = sha256()
    BLOCKSIZE = 65536
    with open(file_path, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    return hasher.hexdigest()

def read_subfolders(basepath_key: str, folders: [str], db):
    for folder in folders:
        path_base = paths[basepath_key]
        path_joined = path.join(paths[basepath_key],folder)
        debug('Reading dataset '+path_joined)
        # Finding location in order to create relative paths later
        basepath_end_loc =len(path_base)+1
        for directory, subdirs, files in walk(path_joined):
            for file_name in files:
                file_total_path = path.join(directory, file_name)
                db.save({
                    'sha2_hex': calculate_hash(file_total_path), \
                    'path_relative': file_total_path[basepath_end_loc:], \
                    'path_base': basepath_key, \
                    'timestamp_edit': path.getmtime(file_total_path)})

if __name__ == '__main__':
    # Timing
    from time import time
    time1 = time()
    # Read folders and save result in DB
    read_subfolders('EPLAN_DATA', ['Documents'], db)
    print(db.len())
    db.cursor.execute('DELETE FROM cache;')
    print(db.len())
    print('Execution time:', time()-time1, 'seconds')
