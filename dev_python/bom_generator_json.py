"""
# Usage:
bom_generator_json -i <inputfile.XML> -o <outputfile.json>

# Description:
Reads partlist from EPLAN in .xml format (Bill of materials navigator, right mouse click, Export...)

Generates a bill of material in .json format.

"""

from voluptuous import All, ExactSequence, Coerce, Error, IsFile, Length, REMOVE_EXTRA, Schema
import xml.etree.ElementTree as ET
import pandas as pd

def bom_generator(input_path_bom_xml, output_path_bom_json):
    # Read XML
    tree = ET.parse(input_path_bom_xml)
    partList = tree.getroot()
    parts_valid = []
    parts_invalid = []

    # Define schema for validation
    schema = Schema({
        'P_FUNC_IDENTDEVICETAG': str,
        'P_ARTICLEREF_PARTNO': All(str, Length(min=1)),
        'P_ARTICLE_QUANTITY_IN_PROJECT_UNIT': Coerce(float),
        'P_ARTICLEREF_CABLE_LENGTH_SUM': Coerce(float)
        }, required=True, extra=REMOVE_EXTRA)

    column_labels = {
        'P_FUNC_IDENTDEVICETAG': 'eplan.devicetag',
        'P_ARTICLEREF_PARTNO': 'eplan.partnr',
        'P_ARTICLE_QUANTITY_IN_PROJECT_UNIT': 'art.quantity',
        'P_ARTICLEREF_CABLE_LENGTH_SUM': 'art.length'}

    # Count accepted lines (i) and rejected lines (j)
    i=0
    j=0

    # Loop and validate
    for element in partList:
        if(element.tag == 'device'):
            for part in element:
                try:
                    schema(part.attrib)
                except Error as e:
                    parts_invalid.append(element.attrib['P_ARTICLEREF_IDENTNAME']+': '+str(e))
                    j=j+1 # Count invalid parts
                else:
                    parts_valid.append(schema(part.attrib))
                    i=i+1 # Count valid parts
    df = pd.DataFrame(parts_valid)

    # Change labels
    df.rename(columns=column_labels, inplace=True)

    # Write to file
    df.to_json(path_or_buf=output_path_bom_json, orient='records')

    print('Export finished, ',i,'elements exported,', j, 'elements ignored.')

if __name__=='__main__':
    import sys, logging
    # Rules for data input
    schema = Schema(ExactSequence([str, '-i', IsFile(str), '-o', str]))
    try:
        args=schema(sys.argv)
    except Error as e:
        # Print usage tips and exit if input is not valid
        logging.critical('tips for correct usage: bom_generator -i <inputfile.xml> -o <outputfile.json>')
        exit(2)
    else:
        # Input is validated. Calling function.
        arg0, arg1, input_path_bom_xml, arg2, output_path_bom_json = args
        bom_generator(input_path_bom_xml, output_path_bom_json)
