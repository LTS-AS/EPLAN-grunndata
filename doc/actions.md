# EPLAN actions

Målet med dette notatet er å komme til et åpent grensesnitt som prosjektledere, konstruktører og administratorer kan forholde seg til.

Det kan skrives mye om hvorfor prinsippet "fail fast" er viktig. Kostnaden ved å fikse på småfeil blir overaskende stor hvis mange er involvert. De fleste slurvefeil bør kunne fanges opp ved innsjekk.

## Init project
Prosjektleder kan kjøre denne rutinen.

Input:

- Prosjektnummer
- Prosjektnavn
- Nødvendige disipliner
- Annen info som konstruktører trenger for å kunne fullføre et prosjekt

Output:

- Nytt GIT repo
- En mappe per disiplin med nødvendig input
- Et "issue" per disiplin som konstruktører kan starte å arbeide med

## Check out

En kunstruktør kan se sine oppgaver som "Issue". Hvis konstruktøren ønsker å starte arbeid på et prosjekt må det sjekkes ut. Ved utsjekk flagges det at noen har startet på oppgaven og nødvendige data kopieres til brukerens arbeidsstasjon.

- Nødvendig info fra prosjektleder
- Basisprosjekt med info fra prosjektleder fylt inn
- Skript som trigger eksporter som trengs for automatisk testing.

## Check in

Når en konstruktør sjekker inn et prosjekt kjøres automatiske tester på prosjektet. Hvis testene blir godkjent blir prosjektet flettet inn i "master".

Hvis den automatiske testen feiler får kunstruktøren beskjed med en gang før prosjektet går til andre brukere. Hvis testene er OK kan prosjektet gå til neste fase.

### Normalvariant, sjekk in prosjekt

- Lag prosjektbackup
- Eksporter prosjektinstillinger
- Eksporter BOM
- Eksporter produksjonsunderlag
- Sjekk inn og slett lokal kopi

### Spesialvariant, sjekk in makroprosjekt

- Lag prosjektbackup
- Eksporter prosjektinstillinger
- Generer makroer
- Sjekk inn og slett lokal kopi

### Spesialvariant, sjekk in basisprosjekt

- Lag prosjektbackup
- Eksporter prosjektinstillinger
- Generer basisprosjekt (.zw9)
- Sjekk inn og slett lokal kopi