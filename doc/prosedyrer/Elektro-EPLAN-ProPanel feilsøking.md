# Maskineksport - Montaseflater blir ikke med

Velg **View, Drilling view** for å få en tidlig tilbakemelding. Hullbilde skal synes gjennom komponenter.

https://www.eplan.help/en-US/Infoportal/Content/Plattform/2.9/EPLAN_Help.htm#htm/ncgui_k_prinzip.htm

- Montasjeflaten må være merket med **Machining**
- Komponenter må ha flagget **Item requires holes in mounting surface**
- Field size må være definert
- Det eksporteres hull for disse Item types: 
  - Floor sheet, general
  - Flange plate
  - Side panel for top module
  - Cover, general
  - Mounting panel, general
  - Cable patching space, side panel
  - Socket orifice
  - Door, general
  - Module panel
  - Partition, general
  - Panel, general
  - Busbar, general
  - Body, general.


# Maskineksport - Komponenter eksporteres skjeivt

Aksene i montasjeflaten kan være skrå. Trykk høyre museknapp på en montasjeflate, mounting surcface, Adjust X-axis. Velg så et start og stoppnkt som går paralelt med ønsket akse.

# Model view - Scheme for automatic dimensioning

Det kommer ikke opp mål automatisk. Hold musepekeren over komponenten for å se hvilken funksjonsdefinisjon den har.

- Velg en "Basic item" som ikke er hele "layout space".
- Den første komponenttypen i rekken er av en type som ikke står i skjemaet.
  - Ta med den nye komponenten i skjemaet ELLER
  - Flytt komponenten en tidels millimmeter slik at de ikke står side ved side lenger.
- En annen komponent som står ved siden av får målsetting istedet
 - Ser ut som at junksjonedefinisjonen "Terminal" står først i rekken når noe i gruppen skal få målsetting.

 # Model view - Item labeling

- TRØBBEL? Har sett noen tilfeller av at item labeling ikke forsvinner. Valgte en annen montasjeflate og gikk tilbake for å få uønskede etiketter til å forsvinne.

# Layout space, få tilbehør til å sette seg på riktig plass automatisk

ULØST! Det er mulig å jumpere til å henge seg på rekkeklemmer automatisk. Jumpere bør være røde for å synes for montør.