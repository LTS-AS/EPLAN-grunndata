# Installasjon av EPLAN ELECTRIC P8 eller EPLAN ProPanel

## Introduksjon

Denne rutinen beskriver installasjon av EPLAN på en windows-maskin med 64-biters operativsystem.

Vi har ofte behov for å ta med PCen ut til kunder. Derfor velger vi å ha databasene i MS-Access filer istedet for i SQL databaser.

## Programvare

- EPLAN ProPanel Professional Version 2.9
- 64-bit Windows 10 Pro
- Filer er plassert på en filserver med navn ws1.
- .net framework 4.5.2
- En variant av MS office. Se tabell under for detaljer.

 Installert programmvare|Resultat|
---|---|
Microsoft Office 2010 64-bit|OK
Microsoft Office 2013 64-bit|OK
Microsoft Office 2016 64-bit|Trolig OK sammen med en variant av Microsoft Access
Microsoft Office 2010 32-bit **OG** Microsoft Access 2016 Runtime 64-bit|OK
Microsoft Office 365 Home 64-bit **OG** Microsoft Access 2013 Runtime 64-bit|OK
Microsoft Office 365 Home 64-bit **OG** Microsoft Access 2016 Runtime 64-bit|Ikke kompatible
Microsoft Office 365 Pro 32-bit **OG** Microsoft Access 2016 Runtime 64-bit|Ikke kompatible
Microsoft Office 365 Pro 32-bit **OG** Microsoft Access 2013 Runtime 64-bit|OK
Microsoft Office 365 Pro 64-bit **OG** Microsoft Access 2016 Runtime 64-bit|Ikke kompatible

## Installasjon

- \\ws1\data\software\EPLAN\Pro Panel 2.9.3.14179\setup.exe
- Velg ProPanel (x64), klikk neste
- Aksepter lisensen, klikk neste
- Klikk Default
- Velg Company code. OBS! vanskelig å gjøre om senere. En del mapper i mappestrukturen vil få dette navnet. Feltet bør ikke stå tomt. Hør med en kollega om du er usikker.
- Velg mm, klikk neste, klikk neste

### Gratulerer! du har installert EPLAN lokalt. Dtte har blitt installert på maskinen

Mappe | Beskrivelse
--- | ---
C:\Program Files\EPLAN | EPLAN applikasjoner
C:\ProgramData\EPLAN\O_Data\Platform Data | Originale systemdata for EPLAN platformen
C:\ProgramData\EPLAN\O_Data\Electric P8 Data | Originale systemdata for EPLAN Electric P8
C:\Users\Public\EPLAN\Settings |  Instillinger for STATION, USER og COMPANY
C:\Users\Public\EPLAN\Data | Systemdata

Legg merke til at alle mapper er plasert på maskinens fellesområde. Det vil si at alle brukere av maskinen har tilgang.

## Tilpassinger for arbeid mot ny filserver

- Monter nettverkspartisjon

```language-cmd
NET USE L: \\ws1\data
```

- Kopier arbeidsmappe fra C:\Users\Public\EPLAN\Data til L:\EPLAN\Data
- Hent scheme....

## Gratulerer! du jobber nå mot en filserver

Nå kan flere jobbe mot samme grunndata, og dra nytte av hverandres forbedringer. Flere brukere kan til til og med åpne samme prosjekt samtidig.

## Konfigurere arbeidsstasjon

- [ ] Sidenavigatoren, slett alle schemes, importer alle schemes
- [ ] Page -> Export -> PDF, slett alle schemes, importer alle schemes
- [ ] Utilities, manufacturing data, export / Labeling
- [ ] Utilities, Scripts, Run..., T_load_scripts.md
- [ ] Options, Settings, User, Management, Shortcut keys, import User+Management+Shortcut keys.xml

## Konfigurasjon for bruker

- [ ] Options, settings, user, Graphical editing, 3D, Rotate when changing viewpoints = False

## Arbeid i felt

- Ta en sikkerhetskopi av C:\Users\Public\EPLAN\Data
- Slett C:\Users\Public\EPLAN\Data
- Kopier L:\EPLAN\Data til C:\Users\Public\EPLAN\Data

## Oppdatering av lisens (for eksempel ved oppgradering)

- Hold inne shift, klikk på EPLAN-ikonet
- Klikk Single-user license, Enter validation code..
- Lim inn valideringskoden og klikk Retreive online

