# Scripts README



## Roadmap for scripts

Oppgave                         | Status
--------------------------------|---
Update and export               | Startet
Test av systemdata              | Ikke Startet
Test av produksjonsunderlag     | Ikke Startet
Project check-in                | Git, GitLab og Visual studio code er det beste vi har
Project check-out               | Git, GitLab og Visual studio code er det beste vi har
System data check-in            | Git, GitLab og Visual studio code er det beste vi har
System data check-out           | Git, GitLab og Visual studio code er det beste vi har

## Update and export

Dette er en oppgave som det er verdt å investere i. En automatisert prosess som genererer
produksjonsunderlag sikkrer kvalitet og reduserer tidsbruk.

Det er en utfordring som dukker opp i flerbrukermiljøer. Noen instillinger må publiseres
til alle EPLAN-brukere for at eksporten skal se lik ut for alle brukere. For eksempel
noen "Page filter" og "PDF export scheme". Det er mulig å lage en add-on som hjelper
til med å synkronisere slike instillinger.

## Test av systemdata

## Test av prosjektdata

## Event name overview

It may exsist a complete list over available events. Please give feedback if you know a better source.

Event name                                  | Description
--------------------------------------------|---
onMainStart                                 | EPLAN startup
onMainEnd                                   | EPLAN shutdown
onActionEnd.String.XPrjActionProjectOpen    | Before project is opening
Eplan.EplApi.OnPostOpenProject              | After project is opened
onActionStart.String.XPrjActionProjectClose | Before project is closing